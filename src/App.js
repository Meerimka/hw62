import React, { Component } from 'react';
import './App.css';
import {Route,Switch,BrowserRouter} from 'react-router-dom';
import MainPage from "./containers/MainPage/MainPage";
import Register from "./components/Register/Register";
import Account from "./components/Account/Account";
class App extends Component {



    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={MainPage}/>
                    <Route path="/register" component={Register} />
                    <Route path="/account" component={Account} />
                    <Route render={()=><h1>Not Found!</h1>} />
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;