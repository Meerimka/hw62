import React, {Fragment} from 'react';
import './Account.css';
import {NavLink} from 'react-router-dom';

const Account =()=>{
    return(
       <Fragment>
           <div className="ProfileName">
               <div className="info">
                   <h3 className="Name">Alice</h3>
                   <p>Status: Online </p>
                   <p>Age: 23</p>
                   <p>Working place: Google </p>
                   <p>Region: Ogashi </p>
                   <p>NumericCode: +985 </p>
                   <p>Telephone number: +9855826587456 </p>
               </div>
               <div >
                   <img alt="profilePhoto" className="profilePhoto" src="https://i.pinimg.com/originals/02/61/0f/02610f0fa69c1a287237317f921df3aa.jpg"/>
                   <NavLink className='Mess' to='/'>Messages</NavLink>
               </div>
           </div>
       </Fragment>
    )
};

export default Account;