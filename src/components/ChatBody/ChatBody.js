import React, {Component} from 'react';
import './ChatBody.css';
import  {NavLink} from 'react-router-dom';

class ChatBody extends Component {

    render() {
        console.log(this.props.message);
        return (
                <div className="Chat">
                    <h3 className="central-text">Chat</h3>
                    <NavLink to='/register'>
                        <button className="send">Register</button>
                    </NavLink>

                    <input  type="text" className="input-block" placeholder="Name" onChange={(e) =>this.props.changeHandler(e)}/>
                    <input type="text" className="input-block block-2" placeholder="Type a text " onChange={(e) =>this.props.textChange(e)}/>
                    <button  className="send" onClick={this.props.send}>Send</button>
                </div>
        )
    }
}

export default ChatBody;