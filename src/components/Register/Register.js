import React, {Component} from 'react';
import './Register.css';
import  {NavLink} from 'react-router-dom';

class Register extends Component{
    state={
        value: '',
        valuePsw: '',
        disable: false,
    }

    NameHandler=(event)=>{
        this.setState({
            value: event.target.value
        })
    }

    PswHandler=(event)=>{
        this.setState({
            valuePsw: event.target.value
        })
    }

    render(){
        let isDisabled = this.state.value !== '' && this.state.valuePsw !== '' ? false : true
        return(
            <form className="Form">
                <div className="FormBox">
                    <h3 className="RegTitle">Registration Form</h3>
                    <input type="text" onChange={this.NameHandler} placeholder="Enter Name" className="Register"/>
                    <input type="text" onChange={this.PswHandler} placeholder="Enter password" className="Register"/>

                    {this.state.value !== '' && this.state.valuePsw !== '' ?
                        <NavLink className='SignIn' to='/account'>Sign in</NavLink> : null}

                </div>
            </form>
        )

    }

}
export  default Register;